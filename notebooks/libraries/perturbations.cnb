{
	"cell_id": 1937552428963877096,
	"cells": [
		{
			"cell_id": 10443723891741714035,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 5141060738058535,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\package{libraries.perturbations}{Defining perturbative symbols with properties, perturbing simple expressions and \nequations with substitutions.}\n\nThis package contains various helper functions to define a standard approach to \\textbf{formally} perturb expressions and \nequations in elementary ways, e.g. formally defining a standard perturbative formalism for Cadabra, inheriting properties from \nunperturbed objects onto perturbed ones, giving perturbative decompositions of expressions and equations and substituting\nperturbative expansions."
				}
			],
			"hidden": true,
			"source": "\\package{libraries.perturbations}{Defining perturbative symbols with properties, perturbing simple expressions and \nequations with substitutions.}\n\nThis package contains various helper functions to define a standard approach to \\textbf{formally} perturb expressions and \nequations in elementary ways, e.g. formally defining a standard perturbative formalism for Cadabra, inheriting properties from \nunperturbed objects onto perturbed ones, giving perturbative decompositions of expressions and equations and substituting\nperturbative expansions."
		},
		{
			"cell_id": 12307761823145380888,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "from copy import copy"
		},
		{
			"cell_id": 1841308867336821902,
			"cell_origin": "client",
			"cell_type": "input",
			"ignore_on_import": true,
			"source": "# Definitions for test cases\n\\partial{#}::PartialDerivative.\nM{#}::LaTeXForm(\"\\hat{\\Psi}\").\nR{#}::LaTeXForm(\"\\hat{\\Phi}\").\n{M_{\\mu\\nu},R_{\\mu\\nu}}::Symmetric.\n{T{#},M{#}}::Depends(\\partial{#})."
		},
		{
			"cell_id": 16236569212680538426,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 15767513077006704087,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\algorithm{defPertSymbol(ex: Ex, pertLabel: str, pertOrder: int) -> Ex}{Returns a new object with the same \nstructure as \\texttt{ex} but with \\texttt{pertOrder} appended to its name, and assigns the properties of \\texttt{ex} to this new object}\n\\textbf{Notes}: \\\\\nThe function inherits only \\verb|Symmetric|, \\verb|TableauSymmetry| and \\verb|Depends| properties: other needed\nproperties can be added by including them at the end of the function where the various property declarations are retreived. \\\\\nAfter the use of \\verb|defPertSymbol($a_{\\mu\\nu}$,'x',2)|, the new symbol \\verb|a2_{\\mu\\nu}| which is returned will be formatted\nin LaTeX as $\\,^{\\scriptscriptstyle{(2)}}a_{\\mu\\nu\\rho}$. \\\\\nThe \\texttt{Weight} and \\texttt{Depends} properties are attached to the object independently of the index structure, i.e. \nthe previous example also defines these properties for\n$\\,^{\\scriptscriptstyle{(2)}}a_{\\mu\\nu\\rho}$, and $\\,^{\\scriptscriptstyle{(2)}}a_{\\mu}^{\\nu}$, etc..."
				}
			],
			"hidden": true,
			"source": "\\algorithm{defPertSymbol(ex: Ex, pertLabel: str, pertOrder: int) -> Ex}{Returns a new object with the same \nstructure as \\texttt{ex} but with \\texttt{pertOrder} appended to its name, and assigns the properties of \\texttt{ex} to this new object}\n\\textbf{Notes}: \\\\\nThe function inherits only \\verb|Symmetric|, \\verb|TableauSymmetry| and \\verb|Depends| properties: other needed\nproperties can be added by including them at the end of the function where the various property declarations are retreived. \\\\\nAfter the use of \\verb|defPertSymbol($a_{\\mu\\nu}$,'x',2)|, the new symbol \\verb|a2_{\\mu\\nu}| which is returned will be formatted\nin LaTeX as $\\,^{\\scriptscriptstyle{(2)}}a_{\\mu\\nu\\rho}$. \\\\\nThe \\texttt{Weight} and \\texttt{Depends} properties are attached to the object independently of the index structure, i.e. \nthe previous example also defines these properties for\n$\\,^{\\scriptscriptstyle{(2)}}a_{\\mu\\nu\\rho}$, and $\\,^{\\scriptscriptstyle{(2)}}a_{\\mu}^{\\nu}$, etc..."
		},
		{
			"cell_id": 14216716886337371097,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def defPertSymbol (ex, pertLabel, pertOrder): \n\t# Create the perturbed version of the object by appending pertOrder to its name\n\tpert = copy(ex)\n\tpert.top().name += str(pertOrder)\n\n\t# Create anoter version where the indices are replaced by {#}\n\tpertGeneral = copy(pert)\n\tfor index in pertGeneral.top().indices():\n\t\tindex.erase()\n\tpertGeneral.top().append_child(${#}$)\n\n\t# Get the LaTeX form of ex with no indices\n\texName = copy(ex)\n\tfor index in exName.top().indices():\n\t\tindex.erase()\n\texLaTeX = exName._latex_()\n\n\t# Copy the LaTeX form of ex to pertGeneral, with the pertOrder written prefix and superscript\n\tLaTeXForm(pertGeneral, Ex(rf'\"\\,^{{^{{({pertOrder})}}}}{exLaTeX}\"'))\n\n\t# Assign properties of ex to pert and pertGeneral\n\tWeight(pertGeneral, Ex(f'label={pertLabel}, value={pertOrder}'))\n\tp_sym = Symmetric.get(ex)\n\tif p_sym is not None:\n\t\tp_sym.attach(pert)\n\tp_depends = Depends.get(ex)\n\tif p_depends is not None:\n\t\tp_depends.attach(pertGeneral)\n\tp_tab = TableauSymmetry.get(ex)\n\tif p_tab is not None:\n\t\tp_tab.attach(pert)\n\n\treturn pert"
		},
		{
			"cell_id": 15622674069655742022,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 13133911073763793395,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 7273597838056602669,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M7_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(7)}}\\hat{\\Psi}_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "defPertSymbol($M_{\\mu\\nu}$,'pert',7);"
		},
		{
			"cell_id": 3674480003250114259,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9741389261261208261,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 1653018220436307075,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "2M7_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}2\\,^{\\scriptscriptstyle{(7)}}\\hat{\\Psi}_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "# Testing Inherited Symmetry\nx1 = canonicalise($M7_{\\mu\\nu}+M7_{\\nu\\mu}$);"
		},
		{
			"cell_id": 1102000948007897176,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 10478083911658369441,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 14106895145190446760,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "a \\partial_{\\rho}(M7_{\\mu \\nu})"
						}
					],
					"source": "\\begin{dmath*}{}a \\partial_{\\rho}{\\,^{\\scriptscriptstyle{(7)}}\\hat{\\Psi}_{\\mu \\nu}}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "# Testing Dependencies\nx2 = unwrap($a \\partial_{\\rho}{M7_{\\mu\\nu}}$);"
		},
		{
			"cell_id": 1000105889987527446,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9475733560514400518,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 7010927589796142879,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M7_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(7)}}\\hat{\\Psi}_{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 11620498030212027581,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 18048079010846446511,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "0"
						}
					],
					"source": "\\begin{dmath*}{}0\\end{dmath*}"
				},
				{
					"cell_id": 3135562045427286782,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 9322027968148875105,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M7_{\\mu}^{\\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(7)}}\\hat{\\Psi}_{\\mu}\\,^{\\nu}\\end{dmath*}"
				},
				{
					"cell_id": 11577492607971686097,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 1281252344239437014,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "0"
						}
					],
					"source": "\\begin{dmath*}{}0\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "# Testing Inherited Weight\nx2 = keep_weight($M7_{\\mu\\nu}$,$pert=7$);\nx2 = keep_weight($M7_{\\mu\\nu}$,$pert=5$);\nx3 = keep_weight($M7_{\\mu}^{\\nu}$,$pert=7$);\nx3 = keep_weight($M7_{\\mu}^{\\nu}$,$pert=5$);"
		},
		{
			"cell_id": 3401424880508167200,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 16205145935788365842,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\algorithm{defPertList(ex: Ex, pertLabel: str, maxPertOrder:int) -> List[Ex]}{Call \\texttt{defPertSymbol} on \\texttt{ex} for\nall values of \\texttt{pertOrder} up to and including \\texttt{maxPertOrder}, and return a list of all generated objects}"
				}
			],
			"hidden": true,
			"source": "\\algorithm{defPertList(ex: Ex, pertLabel: str, maxPertOrder:int) -> List[Ex]}{Call \\texttt{defPertSymbol} on \\texttt{ex} for\nall values of \\texttt{pertOrder} up to and including \\texttt{maxPertOrder}, returning a list of all generated objects}"
		},
		{
			"cell_id": 10260948607080745192,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def defPertList (ex, pertLabel, maxPertOrder):\n\tpertList = []\n\tfor i in range(maxPertOrder+1):\n\t\tsymbol_i = defPertSymbol(ex,pertLabel,i) \n\t\tpertList.append(symbol_i)\n\treturn pertList "
		},
		{
			"cell_id": 3754089311911015856,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 11719526550185997284,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 2664904469270580829,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M1_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "x = defPertList($M_{\\mu\\nu}$,'pert',2)\nx[1];"
		},
		{
			"cell_id": 10505334144935070539,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 7975142185860966017,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\algorithm{defPertSum(ex: Ex, exPertList: List[Ex]) -> Ex}{Return an equation with \\texttt{ex} on the left hand side, and a sum\nof the terms in \\texttt{exPertList} on the right hand side i.e. write \\texttt{ex} as a sum of its perturbative components}"
				}
			],
			"source": "\\algorithm{defPertSum(ex: Ex, exPertList: List[Ex]) -> Ex}{Return an equation with \\texttt{ex} on the left hand side, and a sum\nof the terms in \\texttt{exPertList} on the right hand side i.e. write \\texttt{ex} as a sum of its perturbative components}"
		},
		{
			"cell_id": 3389164173577733248,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def defPertSum(ex, exPertList):\n\tpertSum := 0:\n\tfor x in exPertList : pertSum += x\n\treturn $@(ex) = @(pertSum)$"
		},
		{
			"cell_id": 991510506474328830,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 3422116888419566651,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10942730070641293400,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M_{\\mu} = M0_{\\mu} + M1_{\\mu} + M2_{\\mu} + M3_{\\mu}"
						}
					],
					"source": "\\begin{dmath*}{}\\hat{\\Psi}_{\\mu} = \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(3)}}\\hat{\\Psi}_{\\mu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "y1 := M_{\\mu}:\ny2 = defPertList(y1,'pert',3)\ny3 = defPertSum(y1,y2);"
		},
		{
			"cell_id": 8510141417734367037,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 3498081562470174036,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\algorithm{subsPertSums(ex: Ex, partLabel: str, maxPertOrder: int, *pertSums: Ex) -> Ex}{Substitute \n\\texttt{pertSums} into \\texttt{ex}, distribute the result and drop any terms higher than \\texttt{maxPertOrder}}"
				}
			],
			"hidden": true,
			"source": "\\algorithm{subsPertSums(ex: Ex, partLabel: str, maxPertOrder: int, *pertSums: Ex) -> Ex}{Substitute \n\\texttt{pertSums} into \\texttt{ex}, distribute the result and drop any terms higher than \\texttt{maxPertOrder}}"
		},
		{
			"cell_id": 15144951558773099000,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def subsPertSums(ex, pertLabel, maxPertOrder, *pertSums):\n\t# Substitute and distribute over the result\n\tfor pertSum in pertSums: \n\t\tsubstitute(ex, pertSum)\n\tdistribute(ex)\n\n\t# Remove higher order terms\n\tif ex.top().name == r\"\\equals\":\n\t\t# Equation, need to drop left and right hand side separately\n\t\tl, r = lhs(ex), rhs(ex)\n\t\tfor i in range(maxPertOrder+1,2*maxPertOrder+1):\n\t\t\tdrop_weight(l, Ex(f\"{pertLabel}={i}\"))\n\t\t\tdrop_weight(r, Ex(f\"{pertLabel}={i}\"))\n\t\treturn $@(l) = @(r)$\n\telse:\n\t\tfor i in range(maxPertOrder+1,2*maxPertOrder+1):\n\t\t\tdrop_weight(ex, Ex(f\"{pertLabel}={i}\"))\n\t\treturn ex"
		},
		{
			"cell_id": 2189451616160437819,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 4790190655820090575,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12798556016375667923,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M0_{\\mu} R0_{\\nu} + M0_{\\mu} R1_{\\nu} + M0_{\\mu} R2_{\\nu} + M0_{\\mu} R3_{\\nu} + M1_{\\mu} R0_{\\nu} + M1_{\\mu} R1_{\\nu} + M1_{\\mu} R2_{\\nu} + M2_{\\mu} R0_{\\nu} + M2_{\\mu} R1_{\\nu} + M3_{\\mu} R0_{\\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(1)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(2)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(3)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(1)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(2)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(1)}}\\hat{\\Phi}_{\\nu}+\\,^{\\scriptscriptstyle{(3)}}\\hat{\\Psi}_{\\mu} \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Phi}_{\\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "z1 := R_{\\mu}:\nz2 = defPertList(z1,'pert',3)\nz3 = defPertSum(z1,z2)\nw = subsPertSums($M_{\\mu}R_{\\nu}$,'pert',3,y3,z3);"
		},
		{
			"cell_id": 5873196814741241329,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 13168147412532007464,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\algorithm{getEquationPertOrder(ex: Ex, pertLabel: str, pertOrder: int) -> Ex}{Return \\texttt{ex} where all terms of an order \ndifferent to \\texttt{pertOrder} are discarded}"
				}
			],
			"hidden": true,
			"source": "\\algorithm{getEquationPertOrder(ex: Ex, pertLabel: str, pertOrder: int) -> Ex}{Return \\texttt{ex} where all terms of an order \ndifferent to \\texttt{pertOrder} are discarded}"
		},
		{
			"cell_id": 7427182014634848577,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def getEquationPertOrder (ex, pertLabel, pertOrder):\n\tl = keep_weight(lhs(ex), Ex(f\"{pertLabel}={pertOrder}\"))\n\tr = keep_weight(rhs(ex), Ex(f\"{pertLabel}={pertOrder}\"))\n\treturn $@(l) = @(r)$"
		},
		{
			"cell_id": 13063860461650220463,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9656394926651540480,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 1440559109618614661,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M0_{\\mu} + M1_{\\mu} + M2_{\\mu} + M3_{\\mu} = R0_{\\mu} + R1_{\\mu} + R2_{\\mu} + R3_{\\mu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(0)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Psi}_{\\mu}+\\,^{\\scriptscriptstyle{(3)}}\\hat{\\Psi}_{\\mu} = \\,^{\\scriptscriptstyle{(0)}}\\hat{\\Phi}_{\\mu}+\\,^{\\scriptscriptstyle{(1)}}\\hat{\\Phi}_{\\mu}+\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Phi}_{\\mu}+\\,^{\\scriptscriptstyle{(3)}}\\hat{\\Phi}_{\\mu}\\end{dmath*}"
				},
				{
					"cell_id": 15980242573320022186,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 11600845031522772621,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "M2_{\\mu} = R2_{\\mu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{\\scriptscriptstyle{(2)}}\\hat{\\Psi}_{\\mu} = \\,^{\\scriptscriptstyle{(2)}}\\hat{\\Phi}_{\\mu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "m = subsPertSums($M_{\\mu}=R_{\\mu}$,'pert',3,y3,z3);\ngetEquationPertOrder(m,'pert',2);"
		},
		{
			"cell_id": 6650741653813735627,
			"cell_origin": "client",
			"cell_type": "input",
			"source": ""
		}
	],
	"description": "Cadabra JSON notebook format",
	"version": 1.0
}
